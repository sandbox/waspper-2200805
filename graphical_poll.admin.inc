<?php
/**
 * @file
 * Page callback: Graphical poll settings
 *
 * @see graphical_poll_menu()
 */


/**
 * Module settings.
 */
function graphical_poll_admin_settings($form, &$form_state) {
  $form['graphical_poll_validation_method'] = array(
    '#type' => 'select',
    '#title' => t('Validation method'),
    '#default_value' => variable_get('graphical_poll_validation_method', 'string_pattern'),
    '#options' => array(
      'string_pattern' => t('String pattern'),
      'server_headers' => t('Request server headers'),
    ),
    '#description' => t('This method will be used to validate if a choice is valid as picture. <em>String pattern</em> is faster, but less secure than <em>Server headers</em>.'),
    '#required' => TRUE,
  );
  $form['graphical_poll_advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#states' => array(
      'invisible' => array(
        ':input[name="graphical_poll_validation_method"]' => array('value' => 'server_headers'),
      ),
    ),
  );
  $form['graphical_poll_advanced']['graphical_poll_allowed_extensions'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed extensions'),
    '#default_value' => variable_get('graphical_poll_allowed_extensions', 'png, jpg, gif'),
    '#description' => t('Enter all your allowed file extensions, separated by ",". This feature will be used only when validation method is <em>String pattern</em>.'),
  );
  $form['graphical_poll_advanced']['graphical_poll_allowed_protocols'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed protocols'),
    '#default_value' => variable_get('graphical_poll_allowed_protocols', 'http, https, ftp'),
    '#description' => t('Enter all your allowed server protocols, separated by ",". This feature will be used only when validation method is <em>String pattern</em>.'),
  );
  return system_settings_form($form);
}
